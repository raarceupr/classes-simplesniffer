#include "ethernet_packet.h"


// These are the function members you will implement in
// Exercise 2. You must implement ALL of the functions
// whose prototypes are in the ethernet_packet class declaration

ethernet_packet::ethernet_packet() {

}


void ethernet_packet::setEtherDHost(u_char dhost[]){

}


void ethernet_packet::setEtherSHost(u_char shost[]){

}


void ethernet_packet::setEtherType(u_short type){
    ethernet.ether_type = type ;
}


string ethernet_packet::mac2string(u_char srcmac[]){

}


// We intentionally left out some of the function headers
// so that you deduce them from the .h file.
